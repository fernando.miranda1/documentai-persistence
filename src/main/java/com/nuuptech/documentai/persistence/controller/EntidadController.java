package com.nuuptech.documentai.persistence.controller;


import com.nuuptech.documentai.persistence.entity.Entidad;
import com.nuuptech.documentai.persistence.service.IEntidadService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/persistence")
public class EntidadController {
	
	private final IEntidadService entidadService;
	
	@GetMapping
	public ResponseEntity<List<Entidad>> findAll(){
		List<Entidad> entities = entidadService.findAll();
		return ResponseEntity.ok(entities);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Entidad> findById(@PathVariable Long id){
		Entidad entity = entidadService.findById(id);
		return ResponseEntity.ok(entity);
	}

	@PostMapping
	public ResponseEntity<Entidad> update(@RequestBody Entidad obj){
		Entidad entity = entidadService.save(obj);
		return ResponseEntity.status(HttpStatus.CREATED).body(entity);

	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Entidad> update(@PathVariable Long id,
			@RequestBody Entidad obj){
		
		Entidad entity = entidadService.update(obj, id);
		return ResponseEntity.status(HttpStatus.OK).body(entity);
		
	}
	
	@DeleteMapping ("/{id}")
	public ResponseEntity<Void> delete (@PathVariable Long id){
		entidadService.delete(id);
		return new ResponseEntity<> (HttpStatus.NO_CONTENT);
		
	}
}
