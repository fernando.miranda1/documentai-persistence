package com.nuuptech.documentai.persistence.service;

import java.util.List;


public interface IGenericService <T, ID>{
	
	List<T> findAll();
	
	T findById(Long ID);

	T save(T t);
	T update(T t,Long ID );
	
	void delete(Long ID);
}
