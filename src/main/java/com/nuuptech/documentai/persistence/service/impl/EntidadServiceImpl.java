package com.nuuptech.documentai.persistence.service.impl;

import com.nuuptech.documentai.persistence.entity.Entidad;
import com.nuuptech.documentai.persistence.exception.NotFoundException;
import com.nuuptech.documentai.persistence.repository.IEntidadRepository;
import com.nuuptech.documentai.persistence.service.IEntidadService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntidadServiceImpl implements IEntidadService {
	
	private final IEntidadRepository entidadRepository;
	
	
	public EntidadServiceImpl(IEntidadRepository entidadRepository) {
		this.entidadRepository = entidadRepository;
	}

	@Override
	public List<Entidad> findAll() {
		
		return entidadRepository.findAll();
	}

	@Override
	public Entidad findById(Long id) {
		return entidadRepository.findById(id).orElseThrow( ()-> new NotFoundException("Id Not Found: "+id));
	}

	@Override
	public Entidad save(Entidad entidad) {
		return entidadRepository.save(entidad);
	}

	@Override
	public Entidad update(Entidad entity, Long id) {
	Entidad entidad = findById(id);
		entidad.setJsonData(entity.getJsonData());
		return entidadRepository.save(entidad);
	}

	@Override
	public void delete(Long id) {
		findById(id);
		entidadRepository.deleteById(id);
		
	}

}
