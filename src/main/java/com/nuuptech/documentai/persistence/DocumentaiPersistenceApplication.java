package com.nuuptech.documentai.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentaiPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentaiPersistenceApplication.class, args);
	}

}
