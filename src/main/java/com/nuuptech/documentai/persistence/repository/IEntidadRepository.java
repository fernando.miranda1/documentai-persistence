package com.nuuptech.documentai.persistence.repository;


import com.nuuptech.documentai.persistence.entity.Entidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEntidadRepository extends JpaRepository<Entidad, Long>{

}
